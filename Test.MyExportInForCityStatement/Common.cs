﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Url
{
    public static class Common
    {
        public static string GBKToISO(string target)
        {
            return Encoding.GetEncoding("iso8859-1").GetString(Encoding.GetEncoding("GBK").GetBytes(target));
        }
    }
}
