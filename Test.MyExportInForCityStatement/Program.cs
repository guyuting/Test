﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoufunLab.Framework;
using SoufunLab.Framework.Data;
using Test.BLL;
using Test.Common;
using Mo = Test.Model.Tool;
using Bl = Test.BLL.Tool;

namespace Test.MyExportInForCityStatement
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ExportInCityIDs(@"D:\id.xlsx");

                //ExportInHouseGoal(@"D:\HouseGoal.xlsx");

                //ExportInUVGoal(@"D:\UVGoal.xlsx");

                //ExportInGroupAndPrincial(@"D:\GroupAndPrincial.xlsx");
            }
            catch (Exception e)
            {
            }


            Console.WriteLine("ok");
            Console.ReadKey();
        }

        public static DateTime LastDayOfMonth(DateTime datetime)
        {
            return datetime.AddDays(1 - datetime.Day).AddMonths(1).AddDays(-1);
        }

        #region 导入3端CityID

        public static void ExportInCityIDs(string filePath)
        {
            List<Mo.CityIDs> list = Utility.ExportInForCityIDs(filePath);
            Mo.SysCity entity = null;
            list.ForEach(item =>
            {
                dynamic wherePart = new ExpandoObject();
                wherePart.City = item.City;
                entity = Bl.SlSysCity.GetByCity(Config.ConnectionString_Read, wherePart);
                if (entity != null)
                {
                    entity.NewPCCityID = item.PCCityID;
                    entity.NewWAPCityID = item.WAPCityID;
                    entity.NewAPPCityID = item.APPCityID;
                    entity.UpdateTime = DateTime.Now;

                    bool r = Bl.SlSysCity.UpdateByID(Config.ConnectionString_Write, entity);
                }
            });
        }

        #endregion

        #region 导入HouseGoal

        public static void ExportInHouseGoal(string filePath)
        {
            List<Mo.SysCity> inList = Utility.ExportInForHouseGoal(filePath);
            List<Mo.SysCity> cList = Bl.SlSysCity.GetListByIsDelete(Config.ConnectionString_Read);

            Mo.SysCity entity = null;
            List<Mo.SysCity> tempList = null;

            foreach (var item in cList)
            {
                Console.WriteLine(item.City);

                tempList = inList.Where(obj => obj.City == item.City).ToList();
                if (tempList != null && tempList.Count > 0)
                {
                    entity = tempList[0];
                    item.HouseGoal = entity.HouseGoal;
                }
                else
                {
                    item.HouseGoal = null;
                }

                item.UpdateTime = DateTime.Now;
                bool r = Bl.SlSysCity.UpdateByID(Config.ConnectionString_Write, item);
            }
        }

        #endregion

        #region 导入UVGoal

        public static void ExportInUVGoal(string filePath)
        {
            List<Mo.SysCity> inList = Utility.ExportInForUVGoal(filePath);
            List<Mo.SysCity> cList = Bl.SlSysCity.GetListByIsDelete(Config.ConnectionString_Read);

            Mo.SysCity entity = null;
            List<Mo.SysCity> tempList = null;

            foreach (var item in cList)
            {
                Console.WriteLine(item.City);

                tempList = inList.Where(obj => obj.City == item.City).ToList();
                if (tempList != null && tempList.Count > 0)
                {
                    entity = tempList[0];
                    item.UVGoal = entity.UVGoal;
                }
                else
                {
                    item.UVGoal = null;
                }

                item.UpdateTime = DateTime.Now;
                bool r = Bl.SlSysCity.UpdateByID(Config.ConnectionString_Write, item);
            }
        }

        #endregion

        #region 导入GroupAndPrincial

        public static void ExportInGroupAndPrincial(string filePath)
        {
            List<Mo.SysCity> inList = Utility.ExportInForGroupAndPrincial(filePath);
            List<Mo.SysCity> cList = Bl.SlSysCity.GetListByIsDelete(Config.ConnectionString_Read);

            Mo.SysCity entity = null;
            List<Mo.SysCity> tempList = null;

            foreach (var item in cList)
            {
                Console.WriteLine(item.City);

                tempList = inList.Where(obj => obj.City == item.City).ToList();
                if (tempList != null && tempList.Count > 0)
                {
                    entity = tempList[0];
                    item.UVGoal = entity.UVGoal;
                    item.CityGroup = entity.CityGroup;
                    item.Principal = entity.Principal;
                }
                else
                {
                    item.UVGoal = null;
                    item.CityGroup = null;
                    item.Principal = null;
                }

                item.UpdateTime = DateTime.Now;
                bool r = Bl.SlSysCity.UpdateByID(Config.ConnectionString_Write, item);
            }
        }

        #endregion
    }
}
