﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Redis;

namespace Test.MyRedis
{
    class Program
    {
        static void Main(string[] args)
        {
            RedisClient client = new RedisClient("192.168.109.250", 6379);
            client.AddItemToSortedSet("test", "1");
            client.AddItemToSortedSet("test", "2");
            client.AddItemToSortedSet("test", "3");
            client.AddItemToSortedSet("test", "4");

            List<string> list = client.GetAllItemsFromSortedSet("test");
            list.ForEach(item => { Console.WriteLine(item); });
            Console.ReadKey();
        }
    }
}
