﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using SoufunLab.Framework;
using Test.Common;
using Bl = Test.BLL.Click;
using Mo = Test.Model.Click;

namespace Test.MyExportInForProject
{
    class Program
    {
        static void Main(string[] args)
        {
            //List<Mo.Business> busList = Bl.Business.GetAll(Config.ConnectionString_Read);
            List<Mo.Chanel> chanelList = Bl.Chanel.GetAll(Config.ConnectionString_Read);
            List<Mo.City> cityList = Bl.City.GetAll(Config.ConnectionString_Read);

            List<Mo.ProjectDetail> inList = Utility.ExportInForProject(@"D:/新房详情页项目.xlsx");
            foreach (var item in inList)
            {
                item.ID = SlGuid.NewGuid();
                item.CityID = cityList.Where(city => city.Name == item.CityName).ToList()[0].ID;
                item.ChanelID = chanelList.Where(chanel => chanel.Name == item.ChanelName).ToList()[0].ID;
                item.IsDelete = 0;
                item.CreateUser = "9eab319bcaff437096a938d81d231b35";
                item.UpdateUser = "9eab319bcaff437096a938d81d231b35";
                item.CreateTime = DateTime.Now;
                item.UpdateTime = DateTime.Now;
            }
            List<Mo.PageConfig> pcList = inList.Select(item =>
            {
                return new Mo.PageConfig()
                {
                    ID = SlGuid.NewGuid(),
                    ProjectID = item.ID,
                    IsCheckLog = 1,
                    IsCheckPage = 1,
                    CreateUser = "9eab319bcaff437096a938d81d231b35",
                    UpdateUser = "9eab319bcaff437096a938d81d231b35",
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now
                };
            }).ToList();

            Bl.Project.Add(Config.ConnectionString_Write, inList);
            Bl.PageConfig.Add(Config.ConnectionString_Write, pcList);

            
            Console.WriteLine("ok");
            Console.ReadKey();
        }
    }
}
