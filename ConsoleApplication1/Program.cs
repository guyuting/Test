﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.IO;
using System.Reflection;
using NPOI.HSSF.Util;
using NPOI.XSSF.UserModel;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Enumberable.Range 和 Enumberable.Select

            //IEnumerable<int> squares =
            //    Enumerable.Range(1, 10).Select(x => x * x);

            //foreach (int num in squares)
            //{
            //    Console.WriteLine(num);
            //}

            //Console.ReadKey();

            #endregion

            #region Enumberable.BinarySearch 和 ~按位反码运算（按位求补）

            //List<string> dinosaurs = new List<string>();

            //dinosaurs.Add("Pachycephalosaurus");
            //dinosaurs.Add("Amargasaurus");
            //dinosaurs.Add("Mamenchisaurus");
            //dinosaurs.Add("Deinonychus");

            //Console.WriteLine();
            //foreach (string dinosaur in dinosaurs)
            //{
            //    Console.WriteLine(dinosaur);
            //}

            //Console.WriteLine("\nSort");
            //dinosaurs.Sort();

            //Console.WriteLine();
            //foreach (string dinosaur in dinosaurs)
            //{
            //    Console.WriteLine(dinosaur);
            //}

            //Console.WriteLine("\nBinarySearch and Insert \"Coelophysis\":");
            //int index = dinosaurs.BinarySearch("Coelophysis");
            //if (index < 0)
            //{
            //    dinosaurs.Insert(~index, "Coelophysis");
            //}

            //Console.WriteLine();
            //foreach (string dinosaur in dinosaurs)
            //{
            //    Console.WriteLine(dinosaur);
            //}

            //Console.WriteLine("\nBinarySearch and Insert \"Tyrannosaurus\":");
            //index = dinosaurs.BinarySearch("Tyrannosaurus");
            //if (index < 0)
            //{
            //    dinosaurs.Insert(~index, "Tyrannosaurus");
            //}

            //Console.WriteLine();
            //foreach (string dinosaur in dinosaurs)
            //{
            //    Console.WriteLine(dinosaur);
            //}

            //Console.ReadKey();

            #endregion

            #region 数字验证

            //while (true)
            //{
            //    Console.WriteLine("请输入");
            //    string input = Console.ReadLine();

            //    bool r = Regex.IsMatch(input, @"^(([1-9]+[0-9]*)|([0]{1}))\.[0-9]*[1-9]+[0-9]*$");

            //    Console.WriteLine(r + "\n");
            //}

            #endregion

            #region NPOI基本操作

            //List<Person> pList = new List<Person>()
            //{
            //    new Person("张大", 1),
            //    new Person("张二", 2),
            //    new Person("张三", 3)
            //};

            //using (HSSFWorkbook wk = new HSSFWorkbook())
            //{
            //    using (HSSFSheet sheet = (HSSFSheet)wk.CreateSheet("People"))
            //    {
            //        PropertyInfo[] arrProperty = pList[0].GetType().GetProperties();
            //        HSSFRow titleRow = (HSSFRow)sheet.CreateRow(0);
            //        for (int n = 0; n < arrProperty.Count(); n++)
            //        {
            //            titleRow.CreateCell(n).SetCellValue(arrProperty[n].Name);
            //        }

            //        for (int i = 0; i < pList.Count; i++)
            //        {
            //            Row r = sheet.CreateRow(i + 1);

            //            for (int j = 0; j < arrProperty.Count(); j++)
            //            {
            //                r.CreateCell(j).SetCellValue(arrProperty[j].GetValue(pList[i]).ToString());
            //            }
            //        }
            //        using (FileStream fs = File.OpenWrite("Person.xls"))
            //        {
            //            wk.Write(fs);
            //        }
            //    }
            //}



            //using (FileStream fs = File.OpenRead("Person.xls"))
            //{
            //    using (HSSFWorkbook wk = new HSSFWorkbook(fs))
            //    {
            //        for (int i = 0; i < wk.NumberOfSheets; i++)
            //        {
            //            using (HSSFSheet sheet = (HSSFSheet)wk.GetSheetAt(i))
            //            {
            //                Console.WriteLine("====================={0}=====================", sheet.SheetName);

            //                for (int j = 0; j < sheet.LastRowNum + 1; j++)
            //                {
            //                    HSSFRow r = (HSSFRow)sheet.GetRow(j);

            //                    for (int k = 0; k < r.LastCellNum; k++)
            //                    {
            //                        HSSFCell cell = (HSSFCell)r.GetCell(k);
            //                        switch (cell.CellType)
            //                        {
            //                            case CellType.BLANK:
            //                                break;
            //                            case CellType.BOOLEAN:
            //                                break;
            //                            case CellType.ERROR:
            //                                break;
            //                            case CellType.FORMULA:
            //                                break;
            //                            case CellType.NUMERIC:
            //                                break;
            //                            case CellType.STRING:
            //                                Console.Write("{0} ", cell.StringCellValue);
            //                                break;
            //                            case CellType.Unknown:
            //                                break;
            //                            default:
            //                                break;
            //                        }
            //                    }

            //                    Console.WriteLine();
            //                }
            //            }
            //        }
            //    }
            //}

            #endregion

            #region NPOI Excel导出

            //Dictionary<string, List<CountOfDate>> dic = new Dictionary<string, List<CountOfDate>>();
            //dic.Add("城市1",
            //    new List<CountOfDate>()
            //    {
            //        new CountOfDate() {Datetime = new DateTime(2015, 10, 1), FangAllCount = 11, BangAllCount = 11, AgentAllCount = 11},
            //        new CountOfDate() {Datetime = new DateTime(2015, 10, 2), FangAllCount = 12, BangAllCount = 12, AgentAllCount = 12},
            //        new CountOfDate() {Datetime = new DateTime(2015, 10, 3), FangAllCount = 13, BangAllCount = 13, AgentAllCount = 13}
            //    });
            //dic.Add("城市2",
            //    new List<CountOfDate>()
            //    {
            //        //new CountOfDate() {Datetime = new DateTime(2015, 10, 1), FangAllCount = 21, BangAllCount = 21, AgentAllCount = 21},
            //        new CountOfDate() {Datetime = new DateTime(2015, 10, 2), FangAllCount = 22, BangAllCount = 22, AgentAllCount = 22},
            //        new CountOfDate() {Datetime = new DateTime(2015, 10, 3), FangAllCount = 23, BangAllCount = 23, AgentAllCount = 23}
            //    });
            //dic.Add("城市3",
            //    new List<CountOfDate>()
            //    {
            //        new CountOfDate() {Datetime = new DateTime(2015, 10, 1), FangAllCount = 31, BangAllCount = 31, AgentAllCount = 31},
            //        new CountOfDate() {Datetime = new DateTime(2015, 10, 2), FangAllCount = 32, BangAllCount = 32, AgentAllCount = 32},
            //        new CountOfDate() {Datetime = new DateTime(2015, 10, 3), FangAllCount = 33, BangAllCount = 33, AgentAllCount = 33}
            //    });

            //List<string> dateList = new List<string>() { "2015-10-01", "2015-10-02", "2015-10-03" };

            //Export(dic, dateList);

            #endregion


            Person p = new Person("gyt", 1);
            Console.WriteLine(p.Name.GytToString());



        }

        public static List<string> ExoprtIn(List<string> list)
        {
            using (FileStream fs = File.OpenRead(@"E:\jr.xlsx"))
            {
                IWorkbook wk = new XSSFWorkbook(fs);

                for (int i = 0; i < wk.NumberOfSheets; i++)
                {
                    ISheet sheet = wk.GetSheetAt(i);

                    for (int j = 0; j < sheet.LastRowNum + 1; j++)
                    {
                        IRow r = sheet.GetRow(j);

                        for (int k = 0; k < r.LastCellNum; k++)
                        {
                            ICell cell = r.GetCell(k);
                            switch (cell.CellType)
                            {
                                case CellType.String:
                                    list.Add(cell.StringCellValue);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }

            return list;
        }

        public static void Export(Dictionary<string, List<CountOfDate>> dic, List<string> dateList)
        {
            IWorkbook wb = new HSSFWorkbook();
            ISheet sh = wb.CreateSheet("统计数据");

            #region 设置表头

            //城市
            sh.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 0, 0));

            //第一行
            IRow row0 = sh.CreateRow(0);
            ICell titleOfCityCell = row0.CreateCell(0);
            titleOfCityCell.CellStyle = Getcellstyle(wb);
            titleOfCityCell.SetCellValue("城市");

            int count = dateList.Count;
            for (int n = 0; n < count; n++)
            {
                sh.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 3 * (n + 1) - 2, 3 * (n + 1)));
                ICell curRow0Cell = row0.CreateCell(3 * (n + 1) - 2);
                curRow0Cell.CellStyle = Getcellstyle(wb);
                curRow0Cell.SetCellValue(dateList[n]);
            }

            //第二行
            IRow row1 = sh.CreateRow(1);
            string cellVal = string.Empty;
            for (int n = 0; n < count * 3; n++)
            {
                cellVal = (n + 1) % 3 == 1 ? "房天下房源数" : (n + 1) % 3 == 2 ? "搜房帮房源数" : "新增经纪人数";
                ICell curRow1Cell = row1.CreateCell(n + 1);
                curRow1Cell.CellStyle = Getcellstyle(wb);
                curRow1Cell.SetCellValue(cellVal);
            }

            #endregion

            #region 加载数据

            int rowIndex = 0;
            foreach (var item in dic)
            {
                IRow curRow = sh.CreateRow(rowIndex + 2);
                curRow.CreateCell(0).SetCellValue(item.Key);
                for (int i = 0; i < count; i++)
                {
                    item.Value.ForEach(obj =>
                    {
                        if (dateList[i] == obj.Datetime.ToString("yyyy-MM-dd"))
                        {
                            curRow.CreateCell(i * 3 + 1).SetCellValue(obj.FangAllCount);
                            curRow.CreateCell(i * 3 + 2).SetCellValue(obj.BangAllCount);
                            curRow.CreateCell(i * 3 + 3).SetCellValue(obj.AgentAllCount);
                        }
                    });
                }
                rowIndex++;
            }

            #endregion

            using (FileStream stm = File.OpenWrite(@"e:/myMergeCell.xls"))
            {
                wb.Write(stm);
                Console.WriteLine("提示：创建成功！");
                Console.ReadKey();
            }
        }

        public static ICellStyle Getcellstyle(IWorkbook wb)
        {
            ICellStyle cellStyle = wb.CreateCellStyle();
            cellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
            return cellStyle;
        }

        #region NPOI Excel导出

        //创建一个常用的xls文件
        public static void CreatExcel(Dictionary<string, List<dynamic>> dic)
        {
            IWorkbook wb = new HSSFWorkbook();
            ISheet sh = wb.CreateSheet("统计数据");

            #region 设置单元的宽度

            //设置单元的宽度  
            //sh.SetColumnWidth(0, 15 * 256);
            //sh.SetColumnWidth(1, 35 * 256);
            //sh.SetColumnWidth(2, 15 * 256);
            //sh.SetColumnWidth(3, 10 * 256);

            #endregion

            #region 设置表头

            //城市
            sh.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 0, 0)); //CellRangeAddress（）该方法的参数次序是：开始行号，结束行号，开始列号，结束列号。

            //第一行
            IRow row0 = sh.CreateRow(0);
            ICell icell1top0 = row0.CreateCell(0);
            icell1top0.CellStyle = Getcellstyle(wb, stylexls.水平居中);
            icell1top0.SetCellValue("城市");

            //第二行
            IRow row1 = sh.CreateRow(1);
            string cellVal = string.Empty;
            var dicTemp = dic.FirstOrDefault();
            int count = dicTemp.Value.Count;

            for (int n = 0; n <= count * 2; n++)
            {
                if (n > 0 && n % 2 == 0)
                {
                    sh.SetColumnWidth(n, 13 * 256);
                }
            }
            for (int n = 0; n < count; n++)
            {
                sh.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 2 * n + 1, 2 * n + 2));
                ICell curRow0Cell = row0.CreateCell(2 * n + 1);
                curRow0Cell.CellStyle = Getcellstyle(wb, stylexls.水平居中);
                curRow0Cell.SetCellValue(dicTemp.Value[n].Datetime.ToString("yyyy-MM-dd"));
            }
            for (int n = 0; n < count * 2; n++)
            {
                cellVal = n % 2 == 0 ? "经纪人数" : "当日房源数";
                ICell curRow1Cell = row1.CreateCell(n + 1);
                curRow1Cell.CellStyle = Getcellstyle(wb, stylexls.水平居中);

                curRow1Cell.SetCellValue(cellVal);
            }

            #endregion

            #region 加载数据

            int rowIndex = 0;
            foreach (var item in dic)
            {
                IRow row2 = sh.CreateRow(rowIndex + 2);
                row2.CreateCell(0).SetCellValue(item.Key);
                for (int i = 0; i < item.Value.Count; i++)
                {
                    row2.CreateCell(i * 2 + 1).SetCellValue(item.Value[i].HouseCount);
                    row2.CreateCell(i * 2 + 2).SetCellValue(item.Value[i].AgentCount);
                }
                rowIndex++;
            }

            #endregion

            using (FileStream stm = File.OpenWrite(@"e:/myMergeCell.xls"))
            {
                wb.Write(stm);
                Console.WriteLine("提示：创建成功！");
                Console.ReadKey();
            }
        }

        #region 定义单元格常用到样式的枚举
        public enum stylexls
        {
            水平居中,
            头,
            url,
            时间,
            数字,
            钱,
            百分比,
            中文大写,
            科学计数法,
            默认
        }
        #endregion

        #region 定义单元格常用到样式

        static ICellStyle Getcellstyle(IWorkbook wb, stylexls str)
        {
            ICellStyle cellStyle = wb.CreateCellStyle();

            //定义几种字体  
            //也可以一种字体，写一些公共属性，然后在下面需要时加特殊的  
            IFont font12 = wb.CreateFont();
            font12.FontHeightInPoints = 10;
            font12.FontName = "微软雅黑";


            IFont font = wb.CreateFont();
            font.FontName = "微软雅黑";
            //font.Underline = 1;下划线  


            IFont fontcolorblue = wb.CreateFont();
            fontcolorblue.Color = HSSFColor.OliveGreen.Blue.Index;
            fontcolorblue.IsItalic = true;//下划线  
            fontcolorblue.FontName = "微软雅黑";


            //边框  
            //cellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.DOTTED;
            //cellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.HAIR;
            //cellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.HAIR;
            //cellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.DOTTED;
            //边框颜色  
            cellStyle.BottomBorderColor = HSSFColor.OliveGreen.Blue.Index;
            cellStyle.TopBorderColor = HSSFColor.OliveGreen.Blue.Index;

            //背景图形，我没有用到过。感觉很丑  
            //cellStyle.FillBackgroundColor = HSSFColor.OLIVE_GREEN.BLUE.index;  
            //cellStyle.FillForegroundColor = HSSFColor.OLIVE_GREEN.BLUE.index;  
            cellStyle.FillForegroundColor = HSSFColor.White.Index;
            // cellStyle.FillPattern = FillPatternType.NO_FILL;  
            cellStyle.FillBackgroundColor = HSSFColor.Blue.Index;

            //水平对齐  
            cellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Left;

            //垂直对齐  
            cellStyle.VerticalAlignment = VerticalAlignment.Center;

            //自动换行  
            cellStyle.WrapText = true;

            //缩进;当设置为1时，前面留的空白太大了。希旺官网改进。或者是我设置的不对  
            cellStyle.Indention = 0;

            //上面基本都是设共公的设置  
            //下面列出了常用的字段类型  
            switch (str)
            {
                case stylexls.水平居中:
                    cellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
                    break;
                case stylexls.头:
                    // cellStyle.FillPattern = FillPatternType.LEAST_DOTS;  
                    cellStyle.SetFont(font12);
                    break;
                case stylexls.时间:
                    IDataFormat datastyle = wb.CreateDataFormat();

                    cellStyle.DataFormat = datastyle.GetFormat("yyyy/mm/dd");
                    cellStyle.SetFont(font);
                    break;
                case stylexls.数字:
                    cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("0.00");
                    cellStyle.SetFont(font);
                    break;
                case stylexls.钱:
                    IDataFormat format = wb.CreateDataFormat();
                    cellStyle.DataFormat = format.GetFormat("￥#,##0");
                    cellStyle.SetFont(font);
                    break;
                //case stylexls.url:
                //    fontcolorblue.Underline = 1;
                //    cellStyle.SetFont(fontcolorblue);
                //    break;
                case stylexls.百分比:
                    cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("0.00%");
                    cellStyle.SetFont(font);
                    break;
                case stylexls.中文大写:
                    IDataFormat format1 = wb.CreateDataFormat();
                    cellStyle.DataFormat = format1.GetFormat("[DbNum2][$-804]0");
                    cellStyle.SetFont(font);
                    break;
                case stylexls.科学计数法:
                    cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("0.00E+00");
                    cellStyle.SetFont(font);
                    break;
                case stylexls.默认:
                    cellStyle.SetFont(font);
                    break;
            }
            return cellStyle;
        }

        #endregion

        #endregion

        public static void CreateExcel()
        {
            IWorkbook wb = new HSSFWorkbook();
            ISheet sh = wb.CreateSheet();
            IRow r = sh.CreateRow(0);
            r.CreateCell(0, CellType.String).SetCellValue("城市");
            r.CreateCell(1, CellType.String).SetCellValue("组别");
            r.CreateCell(2, CellType.String).SetCellValue("负责人");
            r.CreateCell(3, CellType.String).SetCellValue("日均UV目标增量");
            r.CreateCell(4, CellType.String).SetCellValue("日均房源目标增量");

            //将文档写到指定位置
            using (FileStream file = new FileStream("D:/Test_NPOI4.xls", FileMode.Create))
            {
                wb.Write(file);
                file.Close();
                file.Dispose();
            }
        }
    }

    public class Person
    {
        public Person()
        {
        }

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public string Name { get; set; }
        public int Age { get; set; }
    }

    public static class StrExt
    {
        public static string GytToString(this string str)
        {
            return str + "---Gyt";
        }
    }

    public class CountOfDate
    {
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 房天下房源总数
        /// </summary>
        public int FangAllCount { get; set; }

        /// <summary>
        /// 搜房帮房源总数
        /// </summary>
        public int BangAllCount { get; set; }

        /// <summary>
        /// 经纪人总数
        /// </summary>
        public int AgentAllCount { get; set; }

        /// <summary>
        /// 日期
        /// </summary>
        public DateTime Datetime { get; set; }
    }
}
