﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.DAL;
using Test.Model;

namespace Test.BLL
{
    public class CityBll
    {
        CityDal cityDal = new CityDal();

        public List<City> GetListByIsDelete(dynamic wherePart)
        {
            return cityDal.GetListByIsDelete(wherePart);
        }
    }
}
