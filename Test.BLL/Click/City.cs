﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using En = Test.Model.Click;
using Da = Test.DAL.Click;

namespace Test.BLL.Click
{
    public class City
    {
        public static List<En.City> GetAll(string databaseConnectionString)
        {
            return Da.City.GetAll(databaseConnectionString);
        }

        public static En.City GetByName(string databaseConnectionString, dynamic wherePart)
        {
            return Da.City.GetByName(databaseConnectionString, wherePart);
        }
    }
}
