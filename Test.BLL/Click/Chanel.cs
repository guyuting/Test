﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using En = Test.Model.Click;
using Da = Test.DAL.Click;

namespace Test.BLL.Click
{
    public class Chanel
    {
        public static List<En.Chanel> GetAll(string databaseConnectionString)
        {
            return Da.Chanel.GetAll(databaseConnectionString);
        }

        public static En.Chanel GetByName(string databaseConnectionString, dynamic wherePart)
        {
            return Da.Chanel.GetByName(databaseConnectionString, wherePart);
        }
    }
}
