﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using En = Test.Model.Click;
using Da = Test.DAL.Click;

namespace Test.BLL.Click
{
    public class Business
    {
        public static List<En.Business> GetAll(string databaseConnectionString)
        {
            return Da.Business.GetAll(databaseConnectionString);
        }
    }
}
