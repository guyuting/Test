﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using En = Test.Model.Click;
using Da = Test.DAL.Click;

namespace Test.BLL.Click
{
    public class PageConfig
    {
        public static void Add(string databaseConnectionString, List<En.PageConfig> entities)
        {
            Da.PageConfig.Add(databaseConnectionString, entities);
        }
    }
}
