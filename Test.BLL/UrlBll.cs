﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.DAL;
using Test.Model;

namespace Test.BLL
{
    public class UrlBll
    {
        UrlDal urlDal = new UrlDal();

        public List<Areas> GetAreaList()
        {
            return urlDal.GetAreaList();
        }
    }
}
