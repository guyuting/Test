﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.DAL;

namespace Test.BLL
{
    public class LoginBll
    {
        LoginDal dal = new LoginDal();

        public bool Login(dynamic wherePart)
        {
            return (dal.Login(wherePart) == null ? false : true);
        }
    }
}
