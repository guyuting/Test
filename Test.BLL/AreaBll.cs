﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.DAL;
using Test.Model;

namespace Test.BLL
{
    public class AreaBll
    {
        AreaDal areaDal = new AreaDal();

        /// <summary>
        /// 根据IsDelete获取列表
        /// </summary>
        /// <param name="wherePart"></param>
        /// <returns></returns>
        public List<Area> GetListByIsDelete(dynamic wherePart)
        {
            return areaDal.GetListByIsDelete(wherePart);
        }
    }
}
