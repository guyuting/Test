﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Da = Test.DAL.Tool;
using En = Test.Model.Tool;

namespace Test.BLL.Tool
{
    public class SlSysCity
    {
        public static DataTable GetTable(string databaseConnectionString)
        {
            return Da.SlSysCity.GetTable(databaseConnectionString);
        }

        #region 查询

        public static List<En.SysCity> GetListByIsDelete(string databaseConnectionString)
        {
            return Da.SlSysCity.GetListByIsDelete(databaseConnectionString);
        }

        public static En.SysCity GetByCity(string databaseConnectionString, dynamic wherePart)
        {
            return Da.SlSysCity.GetByCity(databaseConnectionString, wherePart);
        }

        #endregion

        #region 更新

        public static bool UpdateByID(string databaseConnectionString, En.SysCity entity)
        {
            return Da.SlSysCity.UpdateByID(databaseConnectionString, entity);
        }

        #endregion
    }
}
