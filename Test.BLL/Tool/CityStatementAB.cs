﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using En = Test.Model.Tool;
using Da = Test.DAL.Tool;


namespace Test.BLL.Tool
{
    public class CityStatementAB
    {
        #region 添加

        public static bool Add(string databaseConnectionString, En.CityStatementAB entity)
        {
            return Da.CityStatementAB.Add(databaseConnectionString, entity);
        }

        #endregion
    }
}
