﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.DAL;
using Test.Model;

namespace Test.BLL
{
    public partial class BBSBll
    {
        BBSDal bbsDal = new BBSDal();

        /// <summary>
        /// 列表查询
        /// </summary>
        /// <returns>评论列表</returns>
        public List<BBS> GetList()
        {
            return bbsDal.GetList();
        }

        /// <summary>
        /// 添加评论
        /// </summary>
        /// <param name="bbs">评论对象</param>
        /// <returns>是否成功</returns>
        public bool Add(BBS bbs)
        {
            return bbsDal.Add(bbs) > 0;
        }

        /// <summary>
        /// 删除评论
        /// </summary>
        /// <param name="id">评论id</param>
        /// <returns>是否成功</returns>
        public bool Delete(int id)
        {
            return bbsDal.Delete(id) > 0;
        }
    }
}
