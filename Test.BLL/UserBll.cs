﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.DAL;
using Test.Model;

namespace Test.BLL
{
    public class UserBll
    {
        UserDal userDal = new UserDal();

        /// <summary>
        /// 列表查询 
        /// </summary>
        /// <returns>用户列表</returns>
        public List<User> GetList()
        {
            return userDal.GetList();
        }

        /// <summary>
        /// 根据id查询用户
        /// </summary>
        /// <param name="id">用户id</param>
        /// <returns>用户对象</returns>
        public User GetById(int id)
        {
            return userDal.GetById(id);
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>是否成功</returns>
        public bool Add(User user)
        {
            return userDal.Add(user) > 0;
        }

        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>是否成功</returns>
        public bool Edit(User user)
        {
            return userDal.Edit(user) > 0;
        }

        /// <summary>
        /// 根据id删除用户
        /// </summary>
        /// <param name="id">用户id</param>
        /// <returns>是否成功</returns>
        public bool Delete(int id)
        {
            return userDal.Delete(id) > 0;
        }
    }
}
