﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.DAL
{
    public class LoginDal
    {
        public object Login(dynamic wherePart)
        {
            string sql = @"SELECT TOP 1 [ID] FROM [SlSysUser] WHERE [Username] = @Username AND [IsDelete] = @IsDelete;";

            SqlParameter[] pms = new SqlParameter[] { 
                new SqlParameter("@Username", SqlDbType.NVarChar, 100){ Value = wherePart.Username },
                new SqlParameter("@IsDelete", SqlDbType.Int){ Value = wherePart.IsDelete }
            };

            return SqlHelper.ExecuteScaler(sql, CommandType.Text, pms);
        }
    }
}
