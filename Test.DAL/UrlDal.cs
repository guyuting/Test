﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Model;
using Test.Common;

namespace Test.DAL
{
    public class UrlDal
    {
        /// <summary>
        /// 根据IsDelete获取列表
        /// </summary>
        /// <param name="wherePart"></param>
        /// <returns></returns>
        public List<Areas> GetAreaList()
        {
            List<Areas> list = new List<Areas>();

            string sql = @"
                           SELECT tc.city_name, tc.city_domain, ta.area_name, ta.quanpin
                             FROM cities AS tc
                           LEFT JOIN area AS ta ON tc.city_name = ta.city;";

            using (MySqlDataReader reader = MySqlHelper.ExecuteReader(sql, CommandType.Text))
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Areas model = new Areas();
                        model.city_name = Utility.ISOToGBK(reader[0].ToString());
                        model.city_domain = reader[1].ToString();
                        model.area_name = Utility.ISOToGBK(reader[2].ToString());
                        model.quanpin = reader[3].ToString();

                        list.Add(model);
                    }
                }
            }

            return list;
        }
    }
}
