﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Model;

namespace Test.DAL
{
    public class CityDal
    {
        public List<City> GetListByIsDelete(dynamic wherePart)
        {
            List<City> list = new List<City>();

            string sql = @"
                            SELECT [ID]
                                  ,[Name]
                                  ,[PyShort]
                                  ,[AreaID]
                                  ,[CreateUser]
                                  ,[UpdateUser]
                                  ,[IsDelete]
                                  ,[CreateTime]
                                  ,[UpdateTime]
                              FROM [SlCity]
                              WHERE [IsDelete] = @IsDelete;
                        ";

            SqlParameter[] pms = new SqlParameter[]{
                new SqlParameter("@IsDelete", SqlDbType.Int){ Value = 0 }
            };

            using (SqlDataReader reader = SqlHelper.ExecuteReader(sql, System.Data.CommandType.Text, pms))
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        City model = new City();
                        model.ID = Convert.ToString(reader[0]);
                        model.Name = Convert.ToString(reader[1]);
                        model.PyShort = Convert.ToString(reader[2]);
                        model.AreaID = Convert.ToString(reader[3]);
                        model.CreateUser = Convert.ToString(reader[4]);
                        model.UpdateUser = Convert.ToString(reader[5]);
                        model.IsDelete = Convert.ToInt32(reader[6]);
                        model.CreateTime = Convert.ToDateTime(reader[7]);
                        model.UpdateTime = Convert.ToDateTime(reader[8]);

                        list.Add(model);
                    }
                }
            }

            return list;
        }
    }
}
