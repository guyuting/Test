﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoufunLab.Framework.Data;
using En = Test.Model.Tool;

namespace Test.DAL.Tool
{
    public class CityStatementAB
    {
        #region 添加

        /// <summary>
        /// 增加实体
        /// </summary>
        /// <param name="databaseConnectionString">数据库链接字符串</param>
        /// <param name="entity">实体</param>
        public static bool Add(string databaseConnectionString, En.CityStatementAB entity)
        {
            var sql = @"
                        INSERT INTO [SlBusCityStatementAB]
                               (
                                [ID]
                                ,[A]
                                ,[B]
                                ,[CreateTime]
                                ,[UpdateTime]

                               )
                         VALUES
                               (
                                @ID
                                ,@A
                                ,@B
                                ,@CreateTime
                                ,@UpdateTime
                               )
                    ";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@ID", Value = entity.ID });
            parameters.Add(new SqlParameter() { ParameterName = "@A", Value = entity.A });
            parameters.Add(new SqlParameter() { ParameterName = "@B", Value = entity.B });
            parameters.Add(new SqlParameter() { ParameterName = "@CreateTime", Value = entity.CreateTime });
            parameters.Add(new SqlParameter() { ParameterName = "@UpdateTime", Value = entity.UpdateTime });


            int i = SlDatabase.ExecuteNonQuery(databaseConnectionString, sql, parameters.ToArray());
            return i > 0 ? true : false;
        }

        #endregion
    }
}
