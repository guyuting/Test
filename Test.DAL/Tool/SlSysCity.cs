﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoufunLab.Framework;
using SoufunLab.Framework.Data;
using En = Test.Model.Tool;

namespace Test.DAL.Tool
{
    public class SlSysCity
    {
        public static DataTable GetTable(string databaseConnectionString)
        {
            var sql = @"
                        SELECT
                                [ID]
                                ,[City]
                                ,[PCUV]
                                ,[WAPUV]
                                ,[APPUV]
                                ,[UVGoal]
                                ,[HouseGoal]
                                ,[CityGroup]
                                ,[Principal]
                                ,[A]
                                ,[B]
                                ,[DailyRanking]
                                ,[DateTime]
                         
                        FROM [SlBusCityUV] WITH (NOLOCK)
                        WHERE 
                            1 <> 1;
                    ";

            var dataTable = new DataTable();
            SlDatabase.Fill(databaseConnectionString, sql, dataTable);

            return dataTable;
        }

        #region 查询

        /// <summary>
        /// 通过IsDelete查询实体列表（不存在时，返回null）
        /// </summary>
        /// <param name="databaseConnectionString">数据库链接字符串</param>
        public static List<En.SysCity> GetListByIsDelete(string databaseConnectionString)
        {
            var sql = @"
                        SELECT 
                                [ID]
                                ,[City]
                                ,[Pinyin]
                                ,[Nanbei]
                                ,[CreateTime]
                                ,[UpdateTime]
                                ,[IsDelete]
                                ,[Belong]
                                ,[IsLanding]
                                ,[CityGroup]
                                ,[Principal]
                                ,[UVGoal]
                                ,[HouseGoal]
                                ,[PCCityID]
                                ,[WAPCityID]
                                ,[APPCityID]
                         
                        FROM [SlSysCity] WITH (NOLOCK)
                        WHERE 
                            [IsDelete] = 0
                        ORDER BY [City] collate Chinese_PRC_CS_AS_KS_WS
                    ";

            var dataTable = new DataTable();
            SlDatabase.Fill(databaseConnectionString, sql, dataTable);

            if (dataTable.Rows.Count > 0)
            {
                return dataTable.AsEnumerable().Select(row => new En.SysCity()
                {
                    ID = SlConvert.TryToString(row["ID"], string.Empty),
                    City = SlConvert.TryToString(row["City"], string.Empty),
                    Pinyin = SlConvert.TryToString(row["Pinyin"], string.Empty),
                    Nanbei = SlConvert.TryToString(row["Nanbei"], string.Empty),
                    CreateTime = SlConvert.TryToDateTime(row["CreateTime"], Convert.ToDateTime("1900-01-01")),
                    UpdateTime = SlConvert.TryToDateTime(row["UpdateTime"], Convert.ToDateTime("1900-01-01")),
                    IsDelete = SlConvert.TryToInt32(row["IsDelete"], -1),
                    Belong = SlConvert.TryToString(row["Belong"], string.Empty),
                    IsLanding = SlConvert.TryToInt32(row["IsLanding"], -1),
                    CityGroup = row["CityGroup"] == DBNull.Value ? null : SlConvert.TryToString(row["CityGroup"], string.Empty),
                    Principal = row["Principal"] == DBNull.Value ? null : SlConvert.TryToString(row["Principal"], string.Empty),
                    UVGoal = row["UVGoal"] == DBNull.Value ? null : (int?)SlConvert.TryToInt32(row["UVGoal"], -1),
                    HouseGoal = row["HouseGoal"] == DBNull.Value ? null : (int?)SlConvert.TryToInt32(row["HouseGoal"], -1),
                    PCCityID = row["PCCityID"] == DBNull.Value ? null : SlConvert.TryToString(row["PCCityID"], string.Empty),
                    WAPCityID = row["WAPCityID"] == DBNull.Value ? null : SlConvert.TryToString(row["WAPCityID"], string.Empty),
                    APPCityID = row["APPCityID"] == DBNull.Value ? null : SlConvert.TryToString(row["APPCityID"], string.Empty)

                }).ToList();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 通过City查询实体（不存在时，返回null）
        /// </summary>
        /// <param name="databaseConnectionString">数据库链接字符串</param>
        /// <param name="wherePart">条件部分</param>
        public static En.SysCity GetByCity(string databaseConnectionString, dynamic wherePart)
        {
            var sql = @"
                        SELECT
                                [ID]
                                ,[City]
                                ,[Pinyin]
                                ,[Nanbei]
                                ,[CreateTime]
                                ,[UpdateTime]
                                ,[IsDelete]
                                ,[Belong]
                                ,[IsLanding]
                                ,[CityGroup]
                                ,[Principal]
                                ,[UVGoal]
                                ,[HouseGoal]
                                ,[PCCityID]
                                ,[WAPCityID]
                                ,[APPCityID]
                                ,[NewPCCityID]
                                ,[NewWAPCityID]
                                ,[NewAPPCityID]
                                ,[CityProperty]
                                ,[IsEmphasis]
                         
                        FROM [SlSysCity] WITH (NOLOCK)
                        WHERE 
                            [City] = @City
                        AND [IsDelete] = 0
                    ";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@City", Value = wherePart.City });

            var dataTable = new DataTable();
            SlDatabase.Fill(databaseConnectionString, sql, dataTable, parameters.ToArray());

            if (dataTable.Rows.Count > 0)
            {
                var row = dataTable.Rows[0];
                return new En.SysCity()
                {
                    ID = SlConvert.TryToString(row["ID"], string.Empty),
                    City = SlConvert.TryToString(row["City"], string.Empty),
                    Pinyin = SlConvert.TryToString(row["Pinyin"], string.Empty),
                    Nanbei = SlConvert.TryToString(row["Nanbei"], string.Empty),
                    CreateTime = SlConvert.TryToDateTime(row["CreateTime"], Convert.ToDateTime("1900-01-01")),
                    UpdateTime = SlConvert.TryToDateTime(row["UpdateTime"], Convert.ToDateTime("1900-01-01")),
                    IsDelete = SlConvert.TryToInt32(row["IsDelete"], -1),
                    Belong = SlConvert.TryToString(row["Belong"], string.Empty),
                    IsLanding = SlConvert.TryToInt32(row["IsLanding"], -1),
                    CityGroup = SlConvert.TryToString(row["CityGroup"], string.Empty),
                    Principal = SlConvert.TryToString(row["Principal"], string.Empty),
                    UVGoal = row["UVGoal"] == DBNull.Value ? null : (int?)SlConvert.TryToInt32(row["UVGoal"], -1),
                    HouseGoal = row["HouseGoal"] == DBNull.Value ? null : (int?)SlConvert.TryToInt32(row["HouseGoal"], -1),
                    PCCityID = SlConvert.TryToString(row["PCCityID"], string.Empty),
                    WAPCityID = SlConvert.TryToString(row["WAPCityID"], string.Empty),
                    APPCityID = SlConvert.TryToString(row["APPCityID"], string.Empty),
                    NewPCCityID = SlConvert.TryToString(row["NewPCCityID"], string.Empty),
                    NewWAPCityID = SlConvert.TryToString(row["NewWAPCityID"], string.Empty),
                    NewAPPCityID = SlConvert.TryToString(row["NewAPPCityID"], string.Empty),
                    CityProperty = SlConvert.TryToInt32(row["CityProperty"], -1),
                    IsEmphasis = SlConvert.TryToInt32(row["IsEmphasis"], -1)
                };
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region 更新

        /// <summary>
        /// 通过ID更新实体
        /// </summary>
        /// <param name="databaseConnectionString">数据库链接字符串</param>
        /// <param name="entity">实体</param>
        public static bool UpdateByID(string databaseConnectionString, En.SysCity entity)
        {
            var sql = @"
                        UPDATE [SlSysCity]
                            SET
                                [ID] = @ID
                                ,[City] = @City
                                ,[Pinyin] = @Pinyin
                                ,[Nanbei] = @Nanbei
                                ,[CreateTime] = @CreateTime
                                ,[UpdateTime] = @UpdateTime
                                ,[IsDelete] = @IsDelete
                                ,[Belong] = @Belong
                                ,[IsLanding] = @IsLanding
                                ,[CityGroup] = @CityGroup
                                ,[Principal] = @Principal
                                ,[UVGoal] = @UVGoal
                                ,[HouseGoal] = @HouseGoal
                                ,[PCCityID] = @PCCityID
                                ,[WAPCityID] = @WAPCityID
                                ,[APPCityID] = @APPCityID
                                ,[NewPCCityID] = @NewPCCityID
                                ,[NewWAPCityID] = @NewWAPCityID
                                ,[NewAPPCityID] = @NewAPPCityID
                                ,[CityProperty] = @CityProperty
                                ,[IsEmphasis] = @IsEmphasis

                            WHERE 
                                [ID] = @ID
                    ";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@ID", Value = entity.ID });
            parameters.Add(new SqlParameter() { ParameterName = "@City", Value = entity.City });
            parameters.Add(new SqlParameter() { ParameterName = "@Pinyin", Value = entity.Pinyin });
            parameters.Add(new SqlParameter() { ParameterName = "@Nanbei", Value = entity.Nanbei });
            parameters.Add(new SqlParameter() { ParameterName = "@CreateTime", Value = entity.CreateTime });
            parameters.Add(new SqlParameter() { ParameterName = "@UpdateTime", Value = entity.UpdateTime });
            parameters.Add(new SqlParameter() { ParameterName = "@IsDelete", Value = entity.IsDelete });
            parameters.Add(new SqlParameter() { ParameterName = "@Belong", Value = entity.Belong });
            parameters.Add(new SqlParameter() { ParameterName = "@IsLanding", Value = entity.IsLanding });
            parameters.Add(new SqlParameter() { ParameterName = "@CityGroup", Value = entity.CityGroup ?? (object)DBNull.Value });
            parameters.Add(new SqlParameter() { ParameterName = "@Principal", Value = entity.Principal ?? (object)DBNull.Value });
            parameters.Add(new SqlParameter() { ParameterName = "@UVGoal", Value = entity.UVGoal ?? (object)DBNull.Value });
            parameters.Add(new SqlParameter() { ParameterName = "@HouseGoal", Value = entity.HouseGoal ?? (object)DBNull.Value });
            parameters.Add(new SqlParameter() { ParameterName = "@PCCityID", Value = entity.PCCityID ?? (object)DBNull.Value });
            parameters.Add(new SqlParameter() { ParameterName = "@WAPCityID", Value = entity.WAPCityID ?? (object)DBNull.Value });
            parameters.Add(new SqlParameter() { ParameterName = "@APPCityID", Value = entity.APPCityID ?? (object)DBNull.Value });
            parameters.Add(new SqlParameter() { ParameterName = "@NewPCCityID", Value = entity.NewPCCityID ?? (object)DBNull.Value });
            parameters.Add(new SqlParameter() { ParameterName = "@NewWAPCityID", Value = entity.NewWAPCityID ?? (object)DBNull.Value });
            parameters.Add(new SqlParameter() { ParameterName = "@NewAPPCityID", Value = entity.NewAPPCityID ?? (object)DBNull.Value });
            parameters.Add(new SqlParameter() { ParameterName = "@CityProperty", Value = entity.CityProperty });
            parameters.Add(new SqlParameter() { ParameterName = "@IsEmphasis", Value = entity.IsEmphasis });

            int i = SlDatabase.ExecuteNonQuery(databaseConnectionString, sql, parameters.ToArray());
            return i > 0 ? true : false;
        }

        #endregion
    }
}
