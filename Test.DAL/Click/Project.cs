﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoufunLab.Framework.Data;
using En = Test.Model.Click;

namespace Test.DAL.Click
{
    public class Project
    {
        #region 增加

        /// <summary>
        /// 增加实体列表（非批量拷贝方式，适用于小数据量的写入）
        /// </summary>
        /// <param name="databaseConnectionString">数据库链接字符串</param>
        /// <param name="entities">实体列表</param>
        public static void Add(string databaseConnectionString, List<En.ProjectDetail> entities)
        {
            entities.ForEach(entity =>
            {
                Add(databaseConnectionString, entity);
            });
        }

        /// <summary>
        /// 增加实体
        /// </summary>
        /// <param name="databaseConnectionString">数据库链接字符串</param>
        /// <param name="entity">实体</param>
        public static bool Add(string databaseConnectionString, En.ProjectDetail entity)
        {
            var sql = @"
                        INSERT INTO [SlProject]
                               (
                                [ID]
                                ,[Name]
                                ,[ChanelID]
                                ,[CityID]
                                ,[ProjectShort]
                                ,[ProjectUrl]
                                ,[Encoding]
                                ,[CreateUser]
                                ,[UpdateUser]
                                ,[IsDelete]
                                ,[CreateTime]
                                ,[UpdateTime]

                               )
                         VALUES
                               (
                                @ID
                                ,@Name
                                ,@ChanelID
                                ,@CityID
                                ,@ProjectShort
                                ,@ProjectUrl
                                ,@Encoding
                                ,@CreateUser
                                ,@UpdateUser
                                ,@IsDelete
                                ,@CreateTime
                                ,@UpdateTime
                               )
                    ";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@ID", Value = entity.ID });
            parameters.Add(new SqlParameter() { ParameterName = "@Name", Value = entity.Name });
            parameters.Add(new SqlParameter() { ParameterName = "@ChanelID", Value = entity.ChanelID });
            parameters.Add(new SqlParameter() { ParameterName = "@CityID", Value = entity.CityID });
            parameters.Add(new SqlParameter() { ParameterName = "@ProjectShort", Value = entity.ProjectShort });
            parameters.Add(new SqlParameter() { ParameterName = "@ProjectUrl", Value = entity.ProjectUrl });
            parameters.Add(new SqlParameter() { ParameterName = "@Encoding", Value = entity.Encoding });
            parameters.Add(new SqlParameter() { ParameterName = "@CreateUser", Value = entity.CreateUser });
            parameters.Add(new SqlParameter() { ParameterName = "@UpdateUser", Value = entity.UpdateUser });
            parameters.Add(new SqlParameter() { ParameterName = "@IsDelete", Value = entity.IsDelete });
            parameters.Add(new SqlParameter() { ParameterName = "@CreateTime", Value = entity.CreateTime });
            parameters.Add(new SqlParameter() { ParameterName = "@UpdateTime", Value = entity.UpdateTime });


            int i = SlDatabase.ExecuteNonQuery(databaseConnectionString, sql, parameters.ToArray());
            return i > 0 ? true : false;
        }

        #endregion
    }
}
