﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoufunLab.Framework;
using SoufunLab.Framework.Data;
using En = Test.Model.Click;

namespace Test.DAL.Click
{
    public class City
    {
        #region 查询

        /// <summary>
        /// 查询所有实体列表（不存在时，返回null）
        /// </summary>
        /// <param name="databaseConnectionString">数据库链接字符串</param>
        public static List<En.City> GetAll(string databaseConnectionString)
        {
            var sql = @"
                        SELECT 
                                [ID]
                                ,[Name]
                                ,[PyShort]
                                ,[AreaID]
                                ,[CreateUser]
                                ,[UpdateUser]
                                ,[IsDelete]
                                ,[CreateTime]
                                ,[UpdateTime]
                         
                        FROM [SlCity] WITH (NOLOCK)
                        WHERE [IsDelete] = 0
                    ";

            var dataTable = new DataTable();
            SlDatabase.Fill(databaseConnectionString, sql, dataTable);

            if (dataTable.Rows.Count > 0)
            {
                return dataTable.AsEnumerable().Select(row => new En.City()
                {
                    ID = SlConvert.TryToString(row["ID"], string.Empty),
                    Name = SlConvert.TryToString(row["Name"], string.Empty),
                    PyShort = SlConvert.TryToString(row["PyShort"], string.Empty),
                    AreaID = SlConvert.TryToString(row["AreaID"], string.Empty),
                    CreateUser = SlConvert.TryToString(row["CreateUser"], string.Empty),
                    UpdateUser = SlConvert.TryToString(row["UpdateUser"], string.Empty),
                    IsDelete = SlConvert.TryToInt32(row["IsDelete"], -1),
                    CreateTime = SlConvert.TryToDateTime(row["CreateTime"], Convert.ToDateTime("1900-01-01")),
                    UpdateTime = SlConvert.TryToDateTime(row["UpdateTime"], Convert.ToDateTime("1900-01-01"))

                }).ToList();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 通过Name查询实体（不存在时，返回null）
        /// </summary>
        /// <param name="databaseConnectionString">数据库链接字符串</param>
        /// <param name="wherePart">条件部分</param>
        public static En.City GetByName(string databaseConnectionString, dynamic wherePart)
        {
            var sql = @"
                        SELECT
                                [ID]
                                ,[Name]
                                ,[PyShort]
                                ,[AreaID]
                                ,[CreateUser]
                                ,[UpdateUser]
                                ,[IsDelete]
                                ,[CreateTime]
                                ,[UpdateTime]
                         
                        FROM [SlCity] WITH (NOLOCK)
                        WHERE 
                            [IsDelete] = 0
                        AND [Name] = @Name
                    ";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@Name", Value = wherePart.Name });

            var dataTable = new DataTable();
            SlDatabase.Fill(databaseConnectionString, sql, dataTable, parameters.ToArray());

            if (dataTable.Rows.Count > 0)
            {
                var row = dataTable.Rows[0];
                return new En.City()
                {
                    ID = SlConvert.TryToString(row["ID"], string.Empty),
                    Name = SlConvert.TryToString(row["Name"], string.Empty),
                    PyShort = SlConvert.TryToString(row["PyShort"], string.Empty),
                    AreaID = SlConvert.TryToString(row["AreaID"], string.Empty),
                    CreateUser = SlConvert.TryToString(row["CreateUser"], string.Empty),
                    UpdateUser = SlConvert.TryToString(row["UpdateUser"], string.Empty),
                    IsDelete = SlConvert.TryToInt32(row["IsDelete"], -1),
                    CreateTime = SlConvert.TryToDateTime(row["CreateTime"], Convert.ToDateTime("1900-01-01")),
                    UpdateTime = SlConvert.TryToDateTime(row["UpdateTime"], Convert.ToDateTime("1900-01-01"))

                };
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}
