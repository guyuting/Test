﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoufunLab.Framework;
using SoufunLab.Framework.Data;
using En = Test.Model.Click;

namespace Test.DAL.Click
{
    public class Business
    {
        #region 查询

        /// <summary>
        /// 查询所有实体列表（不存在时，返回null）
        /// </summary>
        /// <param name="databaseConnectionString">数据库链接字符串</param>
        public static List<En.Business> GetAll(string databaseConnectionString)
        {
            var sql = @"
                        SELECT 
                                [ID]
                                ,[Name]
                                ,[CreateUser]
                                ,[UpdateUser]
                                ,[IsDelete]
                                ,[CreateTime]
                                ,[UpdateTime]
                         
                        FROM [SlBusiness] WITH (NOLOCK)
                        WHERE IsDelete = 0
                    ";

            var dataTable = new DataTable();
            SlDatabase.Fill(databaseConnectionString, sql, dataTable);

            if (dataTable.Rows.Count > 0)
            {
                return dataTable.AsEnumerable().Select(row => new En.Business()
                {
                    ID = SlConvert.TryToString(row["ID"], string.Empty),
                    Name = SlConvert.TryToString(row["Name"], string.Empty),
                    CreateUser = SlConvert.TryToString(row["CreateUser"], string.Empty),
                    UpdateUser = SlConvert.TryToString(row["UpdateUser"], string.Empty),
                    IsDelete = SlConvert.TryToInt32(row["IsDelete"], -1),
                    CreateTime = SlConvert.TryToDateTime(row["CreateTime"], Convert.ToDateTime("1900-01-01")),
                    UpdateTime = SlConvert.TryToDateTime(row["UpdateTime"], Convert.ToDateTime("1900-01-01"))

                }).ToList();
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}
