﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoufunLab.Framework.Data;
using En = Test.Model.Click;

namespace Test.DAL.Click
{
    public class PageConfig
    {
        #region 添加

        /// <summary>
        /// 增加实体列表（非批量拷贝方式，适用于小数据量的写入）
        /// </summary>
        /// <param name="databaseConnectionString">数据库链接字符串</param>
        /// <param name="entities">实体列表</param>
        public static void Add(string databaseConnectionString, List<En.PageConfig> entities)
        {
            entities.ForEach(entity =>
            {
                Add(databaseConnectionString, entity);
            });
        }

        /// <summary>
        /// 增加实体
        /// </summary>
        /// <param name="databaseConnectionString">数据库链接字符串</param>
        /// <param name="entity">实体</param>
        public static bool Add(string databaseConnectionString, En.PageConfig entity)
        {
            var sql = @"
                        INSERT INTO [SlPageConfig]
                               (
                                [ID]
                                ,[ProjectID]
                                ,[IsCheckLog]
                                ,[IsCheckPage]
                                ,[CreateUser]
                                ,[UpdateUser]
                                ,[CreateTime]
                                ,[UpdateTime]

                               )
                         VALUES
                               (
                                @ID
                                ,@ProjectID
                                ,@IsCheckLog
                                ,@IsCheckPage
                                ,@CreateUser
                                ,@UpdateUser
                                ,@CreateTime
                                ,@UpdateTime
                               )
                    ";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@ID", Value = entity.ID });
            parameters.Add(new SqlParameter() { ParameterName = "@ProjectID", Value = entity.ProjectID });
            parameters.Add(new SqlParameter() { ParameterName = "@IsCheckLog", Value = entity.IsCheckLog });
            parameters.Add(new SqlParameter() { ParameterName = "@IsCheckPage", Value = entity.IsCheckPage });
            parameters.Add(new SqlParameter() { ParameterName = "@CreateUser", Value = entity.CreateUser });
            parameters.Add(new SqlParameter() { ParameterName = "@UpdateUser", Value = entity.UpdateUser });
            parameters.Add(new SqlParameter() { ParameterName = "@CreateTime", Value = entity.CreateTime });
            parameters.Add(new SqlParameter() { ParameterName = "@UpdateTime", Value = entity.UpdateTime });


            int i = SlDatabase.ExecuteNonQuery(databaseConnectionString, sql, parameters.ToArray());
            return i > 0 ? true : false;
        }

        #endregion
    }
}
