﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.DAL
{
    public class MySqlHelper
    {
        //连接字符串
        private static readonly string conStr = ConfigurationManager.ConnectionStrings["mysql_newhouse_conn_n"].ConnectionString;

        //ExecuteReader
        public static MySqlDataReader ExecuteReader(string sql, CommandType cmdType, MySqlParameter[] pms = null)
        {
            MySqlConnection con = new MySqlConnection(conStr);

            using (MySqlCommand cmd = new MySqlCommand(sql, con))
            {
                cmd.CommandType = cmdType;

                if (pms != null)
                {
                    cmd.Parameters.AddRange(pms);
                }

                try
                {
                    con.Open();
                    return cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch
                {
                    con.Close();
                    con.Dispose();
                    throw;
                }
            }
        }
    }
}
