﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Model;

namespace Test.DAL
{
    public partial class BBSDal
    {
        /// <summary>
        /// 列表查询
        /// </summary>
        /// <returns>评论列表</returns>
        public List<BBS> GetList()
        {
            List<BBS> list = new List<BBS>();
            string sql = "select * from BBS";
            using (SqlDataReader reader = SqlHelper.ExecuteReader(sql, CommandType.Text))
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        BBS model = new BBS();
                        model.Id = reader.GetInt32(0);
                        model.Content = reader.GetString(1);
                        list.Add(model);
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// 添加评论
        /// </summary>
        /// <param name="bbs">评论对象</param>
        /// <returns>影响行数</returns>
        public int Add(BBS bbs)
        {
            string sql = "insert into BBS values(@Content)";
            SqlParameter[] pms = new SqlParameter[] 
            {
                new SqlParameter("@Content", SqlDbType.NVarChar, 4000){ Value = bbs.Content }
            };
            return SqlHelper.ExecuteNonQuery(sql, CommandType.Text, pms);
        }

        /// <summary>
        /// 删除评论
        /// </summary>
        /// <param name="id">评论id</param>
        /// <returns>影响行数</returns>
        public int Delete(int id)
        {
            string sql = "delete from BBS where id = @id";
            SqlParameter[] pms = new SqlParameter[] { 
                new SqlParameter("@id", SqlDbType.Int) { Value = id }
            };
            return SqlHelper.ExecuteNonQuery(sql, CommandType.Text, pms);
        }
    }
}
