﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Model;

namespace Test.DAL
{
    public partial class UserDal
    {
        /// <summary>
        /// 列表查询
        /// </summary>
        /// <returns>用户列表</returns>
        public List<User> GetList()
        {
            List<User> userList = new List<User>();
            string sql = "select * from TestUser;";
            using (SqlDataReader reader = SqlHelper.ExecuteReader(sql, System.Data.CommandType.Text))
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        User model = new User();
                        model.UserID = reader.GetInt32(0);
                        model.UserName = reader.GetString(1);
                        model.UserAge = reader.GetInt32(2);

                        userList.Add(model);
                    }
                }
            }

            return userList;
        }

        /// <summary>
        /// 根据id查询用户
        /// </summary>
        /// <param name="id">用户id</param>
        /// <returns>用户对象</returns>
        public User GetById(int id)
        {
            User model = new User();

            string sql = "select * from TestUser where UserID = @userId;";
            SqlParameter[] pms = new SqlParameter[] { 
                new SqlParameter("@userId", SqlDbType.Int) { Value = id }
            };

            using(SqlDataReader reader = SqlHelper.ExecuteReader(sql, CommandType.Text, pms))
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        model.UserID = reader.GetInt32(0);
                        model.UserName = reader.GetString(1);
                        model.UserAge = reader.GetInt32(2);
                    }
                }
            }

            return model;
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>影响行数</returns>
        public int Add(User user)
        {
            string sql = "insert into TestUser values(@UserName, @UserAge)";
            SqlParameter[] pms = new SqlParameter[] 
            {
                new SqlParameter("@UserName", SqlDbType.NVarChar, 10){ Value = user.UserName },
                new SqlParameter("@UserAge", SqlDbType.Int){ Value = user.UserAge }
            };
            return SqlHelper.ExecuteNonQuery(sql, CommandType.Text, pms);
        }

        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>影响行数</returns>
        public int Edit(User user)
        {
            string sql = "update TestUser set UserAge = @UserAge, UserName = @UserName where UserID = @UserID";
            SqlParameter[] pms = new SqlParameter[] 
            {
                new SqlParameter("@UserAge", SqlDbType.Int){ Value = user.UserAge },
                new SqlParameter("@UserName", SqlDbType.NVarChar, 10){ Value = user.UserName },
                new SqlParameter("@UserID", SqlDbType.Int) { Value = user.UserID }
            };
            return SqlHelper.ExecuteNonQuery(sql, CommandType.Text, pms);
        }

        /// <summary>
        /// 根据id删除用户
        /// </summary>
        /// <param name="id">用户id</param>
        /// <returns>影响行数</returns>
        public int Delete(int id)
        {
            string sql = "delete from TestUser where UserID = @UserID";
            SqlParameter[] pms = new SqlParameter[] 
            {
                new SqlParameter("@UserID", SqlDbType.Int){ Value = id }
            };
            return SqlHelper.ExecuteNonQuery(sql, CommandType.Text, pms);
        }
    }
}
