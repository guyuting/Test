﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Model;

namespace Test.DAL
{
    public class AreaDal
    {
        /// <summary>
        /// 根据IsDelete获取列表
        /// </summary>
        /// <param name="wherePart"></param>
        /// <returns></returns>
        public List<Area> GetListByIsDelete(dynamic wherePart)
        {
            List<Area> list = new List<Area>();

            string sql = @"
                            SELECT [ID]
                                  ,[Name]
                                  ,[CreateUser]
                                  ,[UpdateUser]
                                  ,[ShowOrder]
                                  ,[IsDelete]
                                  ,[CreateTime]
                                  ,[UpdateTime]
                              FROM [SlArea]
                             WHERE [IsDelete] = 0;
                        ";

            SqlParameter[] pms = new SqlParameter[] { 
                new SqlParameter("@IsDelete", SqlDbType.Int){ Value = wherePart.IsDelete }
            };

            using (SqlDataReader reader = SqlHelper.ExecuteReader(sql, CommandType.Text, pms))
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Area model = new Area();
                        model.ID = reader[0].ToString();
                        model.Name = reader[1].ToString();
                        model.CreateUser = reader[2].ToString();
                        model.UpdateUser = reader[3].ToString();
                        model.ShowOrder = Convert.ToInt32(reader[4]);
                        model.IsDelete = Convert.ToInt32(reader[5]);
                        model.CreateTime = Convert.ToDateTime(reader[6]);
                        model.UpdateTime = Convert.ToDateTime(reader[7]);

                        list.Add(model);
                    }
                }
            }

            return list;
        }
    }
}
