﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test.Model;
using Test.BLL;
using System.Dynamic;
using Test.UI.Common;

namespace Test.UI.Controllers
{
    public class LoginController : Controller
    {
        LoginBll loginBll = new LoginBll();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            var actionResult = default(ActionResult);

            Login inModel = new Login();
            this.UpdateModel<Login>(inModel);

            #region 参数验证
            if (string.IsNullOrEmpty(inModel.Username))
            {
                throw new Exception("用户名不能为空");
            }
            if (string.IsNullOrEmpty(inModel.Password))
            {
                throw new Exception("密码不能为空");
            }
            #endregion

            dynamic wherePart = new ExpandoObject();
            wherePart.Username = inModel.Username;
            wherePart.IsDelete = 0;

            if (loginBll.Login(wherePart) && inModel.Password == "1")
            {
                //保存Session
                Session["UserName"] = inModel.Username;

                actionResult = new JsonResult()
                {
                    Data = new {
                        Message = StandardMessage.Success
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                actionResult = new JsonResult()
                {
                    Data = new {
                        Message = StandardMessage.Error
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }

            return actionResult;
        }

        public ActionResult Exit()
        {
            Session.Clear();
            Session.Abandon();

            return RedirectToAction("Index", "Login");
        }
    }
}