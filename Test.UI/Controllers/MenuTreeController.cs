﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Test.BLL;
using Test.Model;
using Test.UI.Common;

namespace Test.UI.Controllers
{
    public class MenuTreeController : Controller
    {
        AreaBll areaBll = new AreaBll();
        CityBll cityBll = new CityBll();

        [MyAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 根据IsDelete获取大区列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetTreeByIsDelete()
        {
            var actionResult = default(ActionResult);

            try
            {
                dynamic wherePart = new ExpandoObject();
                wherePart.IsDelete = 0;

                //获取大区列表
                List<Area> areaList = areaBll.GetListByIsDelete(wherePart);
                //获取城市列表
                List<City> cityList = cityBll.GetListByIsDelete(wherePart);

                string xml = string.Empty;
                if (areaList != null)
                {
                    //拼接树
                    xml = @"
                                <tree id='0'>
                                    <item text='全部' id='1' child='1' im0='folderClosed.gif' im1='folderClosed.gif' im2='folderClosed.gif' open='1'>
                                        {0}
                                    </item>
                                </tree>
                            ";

                    xml = string.Format(xml, GetTree.GetCityTree(areaList, cityList));

                    //actionResult = new ContentResult()
                    //{
                    //    Content = new JavaScriptSerializer().Serialize(new
                    //    {
                    //        Message = StandardMessage.Success,
                    //        XmlOfAreaTree = xml
                    //    })
                    //};

                    actionResult = new JsonResult()
                    {
                        Data = new
                        {
                            Message = StandardMessage.Success,
                            XmlOfAreaTree = xml
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                else
                {
                    actionResult = new ContentResult()
                    {
                        Content = new JavaScriptSerializer().Serialize(new
                        {
                            Message = StandardMessage.Error
                        })
                    };
                }
            }
            catch (Exception e)
            {
                actionResult = new ContentResult()
                {
                    Content = new JavaScriptSerializer().Serialize(new
                    {
                        Message = e.Message
                    })
                };
            }

            return actionResult;
        }
    }
}
