﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test.BLL;
using Test.Model;

namespace Test.UI.Controllers
{
    public class BbsController : Controller
    {
        BBSBll bbsBll = new BBSBll();

        public ActionResult Index()
        {
            List<BBS> list = bbsBll.GetList();
            ViewBag.list = list;
            return View();
        }

        /// <summary>
        /// 添加评论
        /// </summary>
        /// <param name="bbs">评论对象</param>
        /// <returns>Json</returns>
        public ActionResult Add(BBS bbs)
        {
            JsonResult j = new JsonResult()
            {
                Data = "no"
            };
            j.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            string content = bbs.Content;

            //评论为空
            if (string.IsNullOrEmpty(content))
            {
                j.Data = "isNull";
                return j;
            }

            content = HttpUtility.UrlDecode(content);
            bbs.Content = content;
            
            bool r = bbsBll.Add(bbs);

            if (r)
            {
                j.Data = "ok";
            }
            return j;
        }

        /// <summary>
        /// 删除评论
        /// </summary>
        /// <param name="id">评论id</param>
        /// <returns>Json</returns>
        public ActionResult Delete(int id)
        {
            string result = "no";
            bool r = bbsBll.Delete(id);

            if (r)
            {
                result = "ok";
            }

            JsonResult j = new JsonResult() 
            {
                Data = result
            };
            j.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            return j;
        }
    }
}
