﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Test.BLL;
using Test.Model;

namespace Test.UI.Controllers
{
    public class UserController : Controller
    {
        UserBll userBll = new UserBll();

        /// <summary>
        /// 用户列表页
        /// </summary>
        /// <returns>用户列表</returns>
        public ActionResult Index()
        {
            List<User> userList = userBll.GetList();
            ViewBag.Model = userList;

            return View();
        }

        /// <summary>
        /// 根据id查询用户
        /// </summary>
        /// <param name="id">用户id</param>
        /// <returns>用户对象</returns>
        public ActionResult GetById(int id)
        {
            User model = new User();
            model = userBll.GetById(id);

            //序列化Json字符串
            //JavaScriptSerializer jss = new JavaScriptSerializer();
            //string jsonData = jss.Serialize(model);

            JsonResult j = new JsonResult()
            {
                Data = model
            };
            j.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            return j;
        }
        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>JSON对象</returns>
        public ActionResult Add(User user)
        {
            bool r = userBll.Add(user);
            string result = "no";

            if (r)
            {
                result = "ok";
            }

            JsonResult j = new JsonResult() 
            {
                Data = result
            };
            j.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            return j;
        }


        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>JSON对象</returns>
        public ActionResult Edit(User user)
        {
            bool r = userBll.Edit(user);
            string result = "no";

            if (r)
            {
                result = "ok";
            }

            JsonResult j = new JsonResult() 
            {
                Data = result
            };
            j.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            return j;
        }

        public ActionResult Delete(int id)
        {
            bool r = userBll.Delete(id);
            string result = "no";

            if (r)
            {
                result = "ok";
            }

            JsonResult j = new JsonResult() 
            {
                Data = result
            };
            j.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            return j;
        }
    }
}
