﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Test.UI.Controllers
{
    public class TestController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Test()
        {
            return View();
        }

        public ActionResult ExcelImport()
        {
            return new JsonResult()
            {
                Data = new
                {
                    Message = "success"
                }
            };
        }

        public ActionResult Uploadify(string someKey, string someOtherKey)
        {
            string result = someKey + someOtherKey;

            return new JsonResult()
            {
                Data = new
                {
                    Message = result
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}
