﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Model;

namespace Test.UI.Common
{
    public class GetTree
    {
        public static string GetCityTree(List<Area> areaList, List<City> cityList)
        {
            string xmlOfTree = string.Empty;

            foreach (var areaItem in areaList)
            {
                List<City> curCityList = cityList.FindAll(c => { return c.AreaID == areaItem.ID; });

                if (curCityList.Count != 0)
                {
                    xmlOfTree += "<item text='" + areaItem.Name + "' id='" + areaItem.ID + "' child='1' im0='folderClosed.gif' im1='folderClosed.gif' im2='folderClosed.gif'>{0}</item>";

                    string xmlOfCity = string.Empty;
                    foreach (var cityItem in curCityList)
                    {
                        xmlOfCity += "<item text='" + cityItem.Name + "' id='" + cityItem.ID + "' child='0' im0='leaf.gif' im1='leaf.gif' im2='leaf.gif'></item>";
                    }
                    xmlOfTree = string.Format(xmlOfTree, xmlOfCity);
                }
                else
                {
                    xmlOfTree += "<item text='" + areaItem.Name + "' id='" + areaItem.ID + "' child='0' im0='folderClosed.gif' im1='folderClosed.gif' im2='folderClosed.gif'></item>";
                }
            }

            return xmlOfTree;
        }
    }
}