﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Model
{
    public class Area
    {
        private string id;
        public string ID 
        {
            get { return id; }
            set { id = value; }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string createuser;
        public string CreateUser
        {
            get { return createuser; }
            set { createuser = value; }
        }

        private string updateuser;
        public string UpdateUser
        {
            get { return updateuser; }
            set { updateuser = value; }
        }

        private DateTime createtime;
        public DateTime CreateTime
        {
            get { return createtime; }
            set { createtime = value; }
        }

        private DateTime updatetime;
        public DateTime UpdateTime
        {
            get { return updatetime; }
            set { updatetime = value; }
        }

        private int showorder;
        public int ShowOrder
        {
            get { return showorder; }
            set { showorder = value; }
        }

        private int isdelete;
        public int IsDelete
        {
            get { return isdelete; }
            set { isdelete = value; }
        }
    }
}
