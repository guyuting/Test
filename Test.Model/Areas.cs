﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Model
{
    public class Areas: Cities
    {
        private string _area_name;
        public string area_name
        {
            get { return _area_name; }
            set { _area_name = value; }
        }

        private string _quanpin;
        public string quanpin
        {
            get { return _quanpin; }
            set { _quanpin = value; }
        }
    }
}
