﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Model.Click
{
    public class Project
    {
        /// <summary>
        /// ID
        /// </summary>
        public string ID { get; set; }


        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// ChanelID
        /// </summary>
        public string ChanelID { get; set; }


        /// <summary>
        /// CityID
        /// </summary>
        public string CityID { get; set; }


        /// <summary>
        /// ProjectShort
        /// </summary>
        public string ProjectShort { get; set; }


        /// <summary>
        /// ProjectUrl
        /// </summary>
        public string ProjectUrl { get; set; }


        /// <summary>
        /// Encoding
        /// </summary>
        public int Encoding { get; set; }


        /// <summary>
        /// CreateUser
        /// </summary>
        public string CreateUser { get; set; }


        /// <summary>
        /// UpdateUser
        /// </summary>
        public string UpdateUser { get; set; }


        /// <summary>
        /// IsDelete
        /// </summary>
        public int IsDelete { get; set; }


        /// <summary>
        /// CreateTime
        /// </summary>
        public DateTime CreateTime { get; set; }


        /// <summary>
        /// UpdateTime
        /// </summary>
        public DateTime UpdateTime { get; set; }
    }
}
