﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Model.Click
{
    public class ProjectDetail : Project
    {
        /// <summary>
        /// BusinessName
        /// </summary>
        public string BusinessName { get; set; }


        /// <summary>
        /// ChanelName
        /// </summary>
        public string ChanelName { get; set; }


        /// <summary>
        /// CityName
        /// </summary>
        public string CityName { get; set; }
    }
}
