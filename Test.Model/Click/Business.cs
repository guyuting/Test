﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Model.Click
{
    public class Business
    {
        /// <summary>
        /// ID
        /// </summary>
        public string ID { get; set; }


        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// CreateUser
        /// </summary>
        public string CreateUser { get; set; }


        /// <summary>
        /// UpdateUser
        /// </summary>
        public string UpdateUser { get; set; }


        /// <summary>
        /// IsDelete
        /// </summary>
        public int IsDelete { get; set; }


        /// <summary>
        /// CreateTime
        /// </summary>
        public DateTime CreateTime { get; set; }


        /// <summary>
        /// UpdateTime
        /// </summary>
        public DateTime UpdateTime { get; set; }
    }
}
