﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Model.Click
{
    public class PageConfig
    {
        /// <summary>
        /// ID
        /// </summary>
        public string ID { get; set; }


        /// <summary>
        /// ProjectID
        /// </summary>
        public string ProjectID { get; set; }


        /// <summary>
        /// IsCheckLog
        /// </summary>
        public int IsCheckLog { get; set; }


        /// <summary>
        /// IsCheckPage
        /// </summary>
        public int IsCheckPage { get; set; }


        /// <summary>
        /// CreateUser
        /// </summary>
        public string CreateUser { get; set; }


        /// <summary>
        /// UpdateUser
        /// </summary>
        public string UpdateUser { get; set; }


        /// <summary>
        /// CreateTime
        /// </summary>
        public DateTime CreateTime { get; set; }


        /// <summary>
        /// UpdateTime
        /// </summary>
        public DateTime UpdateTime { get; set; }
    }
}
