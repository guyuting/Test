﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Model
{
    public class Cities
    {
        private int _city_id;
        public int city_id
        {
            get { return _city_id; }
            set { _city_id = value; }
        }

        private string _city_name;
        public string city_name
        {
            get { return _city_name; }
            set { _city_name = value; }
        }

        private string _city_domain;
        public string city_domain
        {
            get { return _city_domain; }
            set { _city_domain = value; }
        }

        private string _belong;
        public string belong
        {
            get { return _belong; }
            set { _belong = value; }
        }

        private int _sorting;
        public int sorting
        {
            get { return _sorting; }
            set { _sorting = value; }
        }

        //private enum _esfopen;
        //public enum esfopen
        //{
        //    get { return _esfopen; }
        //    set { _esfopen = value; }
        //}

        private string _city_pinyin;
        public string city_pinyin
        {
            get { return _city_pinyin; }
            set { _city_pinyin = value; }
        }
    }
}
