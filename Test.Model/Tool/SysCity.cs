﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Model.Tool
{
    public class SysCity
    {
        /// <summary>
        /// ID
        /// </summary>
        public string ID { get; set; }


        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }


        /// <summary>
        /// Pinyin
        /// </summary>
        public string Pinyin { get; set; }


        /// <summary>
        /// Nanbei
        /// </summary>
        public string Nanbei { get; set; }


        /// <summary>
        /// CreateTime
        /// </summary>
        public DateTime CreateTime { get; set; }


        /// <summary>
        /// UpdateTime
        /// </summary>
        public DateTime UpdateTime { get; set; }


        /// <summary>
        /// IsDelete
        /// </summary>
        public int IsDelete { get; set; }


        /// <summary>
        /// Belong
        /// </summary>
        public string Belong { get; set; }


        /// <summary>
        /// IsLanding
        /// </summary>
        public int IsLanding { get; set; }


        /// <summary>
        /// 组别
        /// </summary>
        public string CityGroup { get; set; }


        /// <summary>
        /// 负责人
        /// </summary>
        public string Principal { get; set; }


        /// <summary>
        /// 日均UV目标增长量
        /// </summary>
        public double? UVGoal { get; set; }


        /// <summary>
        /// 日均房源目标增长量
        /// </summary>
        public double? HouseGoal { get; set; }


        /// <summary>
        /// PC端城市ID
        /// </summary>
        public string PCCityID { get; set; }


        /// <summary>
        /// WAP端城市ID
        /// </summary>
        public string WAPCityID { get; set; }


        /// <summary>
        /// APP端城市ID
        /// </summary>
        public string APPCityID { get; set; }


        /// <summary>
        /// 新房PC端城市ID
        /// </summary>
        public string NewPCCityID { get; set; }


        /// <summary>
        /// 新房WAP端城市ID
        /// </summary>
        public string NewWAPCityID { get; set; }


        /// <summary>
        /// 新房APP端城市ID
        /// </summary>
        public string NewAPPCityID { get; set; }


        /// <summary>
        /// 1:全包属性2:分成属性3:招远属性
        /// </summary>
        public int CityProperty { get; set; }


        /// <summary>
        /// 是否是重点城市（0-否，1-是）
        /// </summary>
        public int IsEmphasis { get; set; }
    }
}
