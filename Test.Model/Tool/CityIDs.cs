﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Model.Tool
{
    public class CityIDs
    {
        public string City { get; set; }
        public string PCCityID { get; set; }
        public string WAPCityID { get; set; }
        public string APPCityID { get; set; }
    }
}
