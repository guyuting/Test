﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Model.Tool
{
    public class CityStatementAB
    {
        /// <summary>
        /// GUID
        /// </summary>
        public string ID { get; set; }


        /// <summary>
        /// A%
        /// </summary>
        public double A { get; set; }


        /// <summary>
        /// B%
        /// </summary>
        public double B { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }


        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UpdateTime { get; set; }
    }
}
