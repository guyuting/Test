﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                List<string> newcodeList = new List<string>(); //多
                List<string> writeList = new List<string>(); //少

                Task t1 = Task.Run(new Action(() =>
                {
                    using (StreamReader read = new StreamReader(@"D:\892.txt"))
                    {
                        while (!read.EndOfStream)
                        {
                            newcodeList.Add(read.ReadLine());
                        }
                    }
                }));

                Task t2 = Task.Run(new Action(() =>
                {
                    using (StreamReader read = new StreamReader(@"D:\891.txt"))
                    {
                        while (!read.EndOfStream)
                        {
                            //Regex.Split(read.ReadLine(), "\t")

                            writeList.Add(read.ReadLine());
                        }
                    }
                }));

                t1.Wait();
                t2.Wait();

                int n = 0;
                string name = "";
                foreach (var item in newcodeList)
                {
                    foreach (var o in newcodeList)
                    {
                        if (o == item)
                        {
                            n++;
                        }
                    }
                    if (n >= 2)
                    {
                        name += item + ",";
                    }

                    n = 0;
                }

                var c = newcodeList.Distinct().Count();

                List<string> list = newcodeList.Where(item => !writeList.Contains(item)).Distinct().ToList();

                StringBuilder sb = new StringBuilder();

                foreach (var item in list)
                {
                    sb.Append(item + ",");
                }

                string r = sb.ToString();

            }
            catch (Exception e)
            { 
            }
        }
    }
}




