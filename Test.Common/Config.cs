﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoufunLab.Framework;
using SoufunLab.Framework.Configuration;

namespace Test.Common
{
    public static class Config
    {
        public static string ConnectionString_Read
        {
            get
            {
                return SlConfig.GetValue<string>("ConnectionString_Read");
            }
        }

        public static string ConnectionString_Write
        {
            get
            {
                return SlConfig.GetValue<string>("ConnectionString_Write");
            }
        }

        public static string test
        {
            get
            {
                return SlConfig.GetValue<string>("test");
            }
        }
    }
}
