﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Mo = Test.Model;

namespace Test.Common
{
    public static class Utility
    {
        public static string ISOToGBK(string target)
        {
            return Encoding.GetEncoding("GBK").GetString(Encoding.GetEncoding("iso8859-1").GetBytes(target));
        }

        /// <summary>
        /// 3端CityID
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static List<Mo.Tool.CityIDs> ExportInForCityIDs(string filePath)
        {
            List<Mo.Tool.CityIDs> list = new List<Mo.Tool.CityIDs>();
            Mo.Tool.CityIDs entity = null;

            using (FileStream fs = File.OpenRead(filePath))
            {
                IWorkbook wk = new XSSFWorkbook(fs);

                for (int i = 0; i < wk.NumberOfSheets; i++)
                {
                    ISheet sheet = wk.GetSheetAt(i);

                    for (int j = 1; j < sheet.LastRowNum + 1; j++)
                    {
                        IRow r = sheet.GetRow(j);
                        entity = new Mo.Tool.CityIDs();

                        for (int k = 0; k < r.LastCellNum; k++)
                        {
                            ICell cell = r.GetCell(k);
                            switch (k)
                            {
                                case 0:
                                    entity.City = cell.StringCellValue;
                                    break;
                                case 1:
                                    entity.PCCityID = cell.NumericCellValue.ToString();
                                    break;
                                case 2:
                                    entity.WAPCityID = cell.NumericCellValue.ToString();
                                    break;
                                case 3:
                                    entity.APPCityID = cell.NumericCellValue.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        list.Add(entity);
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// HouseGoal
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static List<Mo.Tool.SysCity> ExportInForHouseGoal(string filePath)
        {
            List<Mo.Tool.SysCity> list = new List<Mo.Tool.SysCity>();
            Mo.Tool.SysCity entity = null;

            using (FileStream fs = File.OpenRead(filePath))
            {
                IWorkbook wk = new XSSFWorkbook(fs);

                for (int i = 0; i < wk.NumberOfSheets; i++)
                {
                    ISheet sheet = wk.GetSheetAt(i);

                    for (int j = 1; j < sheet.LastRowNum + 1; j++)
                    {
                        IRow r = sheet.GetRow(j);
                        entity = new Mo.Tool.SysCity();

                        for (int k = 0; k < r.LastCellNum; k++)
                        {
                            ICell cell = r.GetCell(k);
                            switch (k)
                            {
                                case 0: //城市
                                    entity.City = cell.StringCellValue.Trim();
                                    break;
                                case 1: //目标日均房源
                                    if (cell.CellType == CellType.String)
                                    {
                                        entity.HouseGoal = Convert.ToInt32(cell.StringCellValue.Trim()) == 0 ? null : (int?)Convert.ToInt32(cell.StringCellValue.Trim());
                                    }
                                    else
                                    {
                                        entity.HouseGoal = Convert.ToInt32(cell.NumericCellValue) == 0 ? null : (int?)Convert.ToInt32(cell.NumericCellValue);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }

                        list.Add(entity);
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// UVGoal
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static List<Mo.Tool.SysCity> ExportInForUVGoal(string filePath)
        {
            List<Mo.Tool.SysCity> list = new List<Mo.Tool.SysCity>();
            Mo.Tool.SysCity entity = null;

            using (FileStream fs = File.OpenRead(filePath))
            {
                IWorkbook wk = new XSSFWorkbook(fs);

                for (int i = 0; i < wk.NumberOfSheets; i++)
                {
                    ISheet sheet = wk.GetSheetAt(i);

                    for (int j = 1; j < sheet.LastRowNum + 1; j++)
                    {
                        IRow r = sheet.GetRow(j);
                        entity = new Mo.Tool.SysCity();

                        for (int k = 0; k < r.LastCellNum; k++)
                        {
                            ICell cell = r.GetCell(k);
                            switch (k)
                            {
                                case 0:
                                    entity.City = cell.StringCellValue;
                                    break;
                                case 1:
                                    if (cell.CellType == CellType.String)
                                    {
                                        entity.UVGoal = Convert.ToInt32(cell.StringCellValue.Trim()) == 0 ? null : (int?)Convert.ToInt32(cell.StringCellValue.Trim());
                                    }
                                    else
                                    {
                                        entity.UVGoal = Convert.ToInt32(cell.NumericCellValue) == 0 ? null : (int?)Convert.ToInt32(cell.NumericCellValue);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }

                        list.Add(entity);
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// GroupAndPrincial
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static List<Mo.Tool.SysCity> ExportInForGroupAndPrincial(string filePath)
        {
            List<Mo.Tool.SysCity> list = new List<Mo.Tool.SysCity>();
            Mo.Tool.SysCity entity = null;

            using (FileStream fs = File.OpenRead(filePath))
            {
                IWorkbook wk = new XSSFWorkbook(fs);

                for (int i = 0; i < wk.NumberOfSheets; i++)
                {
                    ISheet sheet = wk.GetSheetAt(i);

                    for (int j = 1; j < sheet.LastRowNum + 1; j++)
                    {
                        IRow r = sheet.GetRow(j);
                        entity = new Mo.Tool.SysCity();

                        for (int k = 0; k < r.LastCellNum; k++)
                        {
                            ICell cell = r.GetCell(k);
                            switch (k)
                            {
                                case 0:
                                    entity.City = cell.StringCellValue.Trim();
                                    if (entity.City == "乌海")
                                    {
                                        int a = 0;
                                    }
                                    break;
                                case 3:
                                    if (cell.CellType == CellType.String)
                                    {
                                        entity.UVGoal = Convert.ToInt32(cell.StringCellValue.Trim()) == 0 ? null : (int?)Convert.ToInt32(cell.StringCellValue.Trim());
                                    }
                                    else
                                    {
                                        entity.UVGoal = Convert.ToInt32(cell.NumericCellValue) == 0 ? null : (int?)Convert.ToInt32(cell.NumericCellValue);
                                    }
                                    break;
                                case 4:
                                    entity.CityGroup = string.IsNullOrEmpty(cell.StringCellValue.Trim()) ? null : cell.StringCellValue.Trim();
                                    break;
                                case 5:
                                    entity.Principal = string.IsNullOrEmpty(cell.StringCellValue.Trim()) ? null : cell.StringCellValue.Trim();
                                    break;
                                default:
                                    break;
                            }
                        }

                        list.Add(entity);
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Project
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static List<Mo.Click.ProjectDetail> ExportInForProject(string filePath)
        {
            List<Mo.Click.ProjectDetail> list = new List<Mo.Click.ProjectDetail>();
            Mo.Click.ProjectDetail entity = null;

            using (FileStream fs = File.OpenRead(filePath))
            {
                IWorkbook wk = new XSSFWorkbook(fs);

                for (int i = 0; i < wk.NumberOfSheets; i++)
                {
                    ISheet sheet = wk.GetSheetAt(i);

                    for (int j = 1; j < sheet.LastRowNum + 1; j++)
                    {
                        IRow r = sheet.GetRow(j);
                        entity = new Mo.Click.ProjectDetail();

                        for (int k = 0; k < r.LastCellNum; k++)
                        {
                            ICell cell = r.GetCell(k);
                            switch (k)
                            {
                                case 0:
                                    entity.Name = cell.StringCellValue.Trim();
                                    break;
                                case 1:
                                    entity.ProjectShort = cell.StringCellValue.Trim();
                                    break;
                                case 2:
                                    entity.ProjectUrl = cell.StringCellValue.Trim();
                                    break;
                                case 3:
                                    entity.CityName = cell.StringCellValue.Trim();
                                    break;
                                case 4:
                                    entity.BusinessName = cell.StringCellValue.Trim();
                                    break;
                                case 5:
                                    entity.ChanelName = cell.StringCellValue.Trim();
                                    break;
                                case 6:
                                    entity.Encoding = cell.StringCellValue.Trim() == "gb2312" ? 0 : 1;
                                    break;
                                default:
                                    break;
                            }
                        }

                        list.Add(entity);
                    }
                }
            }

            return list;
        }
    }
}
