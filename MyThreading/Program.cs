﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyThreading
{
    public delegate int MyDelegate(int a, int b);

    class Program
    {
        static void Main(string[] args)
        {
            //M1(T2);

            MyDelegate md = T2;
            if (md != null)
            {
                Console.WriteLine(md(1, 2));
            }

            Console.ReadKey();
        }

        private static void M1(MyDelegate methord)
        {
            int a = 1;
            int b = 2;
            Console.WriteLine(methord(a, b));
        }

        public static void T1()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
                Thread.Sleep(500);
            }
        }

        private static int T2(int a, int b)
        {
            return a + b;
        }

        /// <summary>
        /// 获取当前线程的ID
        /// </summary>
        /// <returns></returns>
        public static int GetThreadingID()
        {
            return Thread.CurrentThread.ManagedThreadId;
        }

        /// <summary>
        /// 打印
        /// </summary>
        public static void Print()
        {
            Thread t = new Thread(() =>
            {
                Console.WriteLine("ID: {0}", Thread.CurrentThread.ManagedThreadId);
                Console.WriteLine("Name: {0}", Thread.CurrentThread.Name);
                Console.WriteLine("Priority: {0}", Thread.CurrentThread.Priority);
                Console.WriteLine("ThreadState: {0}", Thread.CurrentThread.ThreadState);

                //for (int i = 0; i < 1000000; i++)
                //{
                //    Console.WriteLine(i);
                //    Thread.Sleep(1000);
                //}
            });

            t.IsBackground = true;
            t.Start();
        }
    }
}
